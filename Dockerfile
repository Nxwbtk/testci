FROM node:20.12.1

COPY app/ /app

WORKDIR /app

RUN npm install

RUN npm run build

ENTRYPOINT [ "npm", "start" ]